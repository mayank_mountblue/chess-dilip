var chessBoardDB = window.localStorage;

class ChessBoard {
    createBoard() {
        let chessBoardData = [];
        for (let i = 0; i < 8; i++) {
            chessBoardData[i] = [];
            for (let j = 0; j < 8; j++) {
                chessBoardData[i][j] = "-1";
            }
        }
        chessBoardDB.setItem("chessDBKey", JSON.stringify(chessBoardData));
        this.storeAllPieces();
    }
    storeAllPieces() {
        var chessBoardData = JSON.parse(localStorage.getItem("chessDBKey"));

        chessBoardData[0][0] = "black-rook1";
        chessBoardData[0][1] = "black-knight1";
        chessBoardData[0][2] = "black-bishop1";
        chessBoardData[0][3] = "black-queen";
        chessBoardData[0][4] = "black-king";
        chessBoardData[0][5] = "black-bishop2";
        chessBoardData[0][6] = "black-knight2";
        chessBoardData[0][7] = "black-rook2";

        chessBoardData[1][0] = "black-pawn1";
        chessBoardData[1][1] = "black-pawn2";
        chessBoardData[1][2] = "black-pawn3";
        chessBoardData[1][3] = "black-pawn4";
        chessBoardData[1][4] = "black-pawn5";
        chessBoardData[1][5] = "black-pawn6";
        chessBoardData[1][6] = "black-pawn7";
        chessBoardData[1][7] = "black-pawn8";

        chessBoardData[6][0] = "white-pawn1";
        chessBoardData[6][1] = "white-pawn2";
        chessBoardData[6][2] = "white-pawn3";
        chessBoardData[6][3] = "white-pawn4";
        chessBoardData[6][4] = "white-pawn5";
        chessBoardData[6][5] = "white-pawn6";
        chessBoardData[6][6] = "white-pawn7";
        chessBoardData[6][7] = "white-pawn8";

        chessBoardData[7][0] = "white-rook1";
        chessBoardData[7][1] = "white-knight1";
        chessBoardData[7][2] = "white-bishop1";
        chessBoardData[7][3] = "white-queen";
        chessBoardData[7][4] = "white-king";
        chessBoardData[7][5] = "white-bishop2";
        chessBoardData[7][6] = "white-knight2";
        chessBoardData[7][7] = "white-rook2";

        chessBoardDB.setItem("chessDBKey", JSON.stringify(chessBoardData));
    }
}